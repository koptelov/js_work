// const obj = {};

// const obj2 = Object({});

// const obj3 = Object.create({});

// console.log(obj);
// console.log(obj2);
// console.log(obj3);

// const cat = {
//   name: "mursik",
//   age: 3,
//   itsBoy: true,
//   toys: ["shar", "fish"],
//   test: () => {
//     console.log("i am dog");
//   },
// };

// // мутация обьекта
// cat.age = 4;

// delete cat.toys;

// console.log(cat);

// let obj = {};

// let obj1 = { 1: "a", 2: "b", 3: "c" };

// console.log(obj);
// console.log(obj1);

// console.log(obj1[1]);
// console.log(obj1[2]);
// console.log(obj1[3]);

// let week = {
//   1: "montag",
//   2: "dinstag",
//   3: "mitwoh",
//   4: "chetverg",
//   5: "fratag",
//   6: "sontag",
//   7: "sonnetntag",
// };
// console.log(week[1]);
// console.log(week[2]);
// console.log(week[3]);
// console.log(week[4]);
// console.log(week[5]);
// console.log(week[6]);
// console.log(week[7]);

// console.log(week);

// let obj = {};

// let obj1 = { 1: "a", 2: "b", 3: "c" };

// console.log(obj);
// console.log(obj1[1]);

// let month = {
//   1: "jenuary",
//   2: "february",
//   3: "marth",
//   4: "apriel",
//   5: "may",
//   6: "june",
//   7: "jule",
//   8: "august",
//   9: "september",
//   10: "oktouber",
//   11: "november",
//   12: "desember",
// };
// console.log(month);

// let user = {
//   name: "Serhii",
//   surname: "Koptielov",
//   Country: "Germania",
// };

// console.log(user);

// let data = {
//   year: "2022",
//   month: "desember",
//   day: "9",
// };
// console.log(data);

// let obj = {
//   "1a": 1,
//   b2: 2,
//   "с-с": 3,
//   "d 4": 4,
//   e5: 5,
// };

// console.log(obj["1a"]);
// console.log(obj.b2);
// console.log(obj["c-c"]);
// console.log(obj["d 4"]);
// console.log(obj.e5);

// let obj = { a: 1, b: 2, c: 3 };

// obj["a"] = "AAA";
// console.log(obj);

// obj["b"] = "bbb";
// obj["c"] = "ccc";

// let obj = { x: 1, y: 2, z: 3 };
// console.log(obj);

// obj.x = 1 ** 2;
// obj.y = 2 ** 2;
// obj.z = 3 ** 2;

// let obj = {};

// obj.a = 1;
// obj.b = 2;
// obj.c = 3;

// console.log(obj);

// let user = {};

// console.log(user);

// user.name = "Serhii";
// user.age = "33";
// user.country = "Germania";

// let user = {};

// user["name"] = "Serhii";
// user["age"] = "33";
// user["country"] = "Germania";

// console.log(user);

// let user = {
//   name: "Serhii",
//   age: "33",
//   country: "USA",
// };

// console.log(user);

// let user1 = {
//   country: "USA",
//   name: "Serhii",
//   age: "33",
// };
// console.log(user1);

// let user3 = {
//   USA: "coutry",
//   Serhii: "coutry",
//   33: "country",
// };

// console.log(user3);

// const car2 = Object.assign({}, car1);

// console.log(car2);

// const car1 = {
//   carName: "Lexus",
//   model: "Rx 350",
// };

// const car2 = {};

// for (let key in car1) {
//   car2[key] = car1[key];
// }

// console.log(car2);
// console.log(car1);

// car2.model = "bobik";

// const class1 = {
//   student1: {
//     html: "96%",
//     css: "85%",
//     js: "43%",
//   },
//   student2: {
//     html: "96%",
//     css: "85%",
//     js: "43%",
//   },
//   student3: {
//     html: "96%",
//     css: "85%",
//     js: "43%",
//   },
// };
// console.log(class1);
// console.log(class1.student1);
// console.log(class1.student2);
// console.log(class1.student3);

// const user = {
//   name: "Petro",
//   lastName: "Poroshenko",
//   profession: "Manager",
// };

// let obj = {
//   a: 1,
//   b: 2,
//   c: 3,
// };

// console.log(obj.length);

// console.log(Object.keys(obj).length);

// let obj = {
//   x: 1,
//   y: 2,
//   z: 3,
//   p: 3,
//   o: 3,
// };
// console.log(Object.keys(obj).length);

// let obj = { a: 1, b: 2, c: 3 };

// let key = "a";

// console.log(obj[key]);

// let obj1 = {
//   x: 1,
//   y: 2,
//   z: 3,
// };

// let key1 = "x";

// console.log(obj1[key]);

// const user = {
//     name: "Petro",
//     lastName: "POROSHENKO",
//     professional: "Manager",
//     newProperty(property, value) {
//         if()
//     }

// }

// let obj1 = {
//   x: 1,
//   y: 2,
//   z: 3,
// };

// let key1 = "x";
// let key2 = "y";

// console.log(obj1["y"]);
// console.log(obj1[key1]);
// console.log(obj1[key2]);

// let obj = { x: 1, y: 2, z: 3 };
// console.log(obj["x"]);

// let obj = { x: 1, y: 2, z: 3 };

// let key = "x";

// console.log(obj[key]);

// let obj = {
//   a: 1,
//   b: 2,
//   c: 3,
// };
// let key = "a";

// console.log(obj[key]);

// let obj = {
//   x: 9,
//   y: 2,
//   z: 3,
// };
// let prop = "x";

// console.log(obj[prop]);

// let obj = {
//   key: 1,
//   y: 2,
//   z: 3,
// };

// let key = "x";
// console.log(obj[key]);

// let key = "x";

// let obj = {
//   [key]: 1,
//   y: 2,
//   z: 3,
// };
// console.log(obj);

// let key1 = "x";
// let key2 = "y";
// let key3 = "z";

// let obj = {
//   [key1]: 1,
//   [key2]: 2,
//   [key3]: 3,
// };

// console.log(obj[key3]);

// let obj = {
//   a: 1,
//   b: 2,
//   c: 3,
// };

// console.log("v" in obj);

// let obj = {
//   x: 1,
//   y: 2,
//   z: 3,
// };

// console.log("x" in obj);
// console.log("w" in obj);

// let obj = {
//   a: 1,
//   b: 2,
//   c: 3,
// };

// delete obj.b;
// delete obj.a;
// delete obj.c;

// console.log(obj);

// let obj = {
//   x: 1,
//   y: 2,
//   z: 3,
// };

// delete obj.x;

// console.log("x" in obj);

// console.log(typeof {});

// console.log(typeof { x: 1, y: 2, z: 3 });
// let obj = { x: 1, y: 2, z: 3 };
// console.log(typeof obj);

// let obj = { x: 1, y: "sdsds", z: true };
// console.log(typeof obj["x"]);
// console.log(typeof obj["y"]);
// console.log(typeof obj["z"]);

// console.log(typeof { x: 1, y: 2, z: 3 });

// console.log(typeof [1, 2, 3]);

// let arr = [1, 2, 3];
// console.log(typeof arr);

// let arr = [1, 2, 3];
// console.log(typeof arr[0]);

// let arr = ["1", "2", "3"];
// console.log(typeof arr[0]);

// console.log(Array.isArray([1, 2, 3]));

// console.log(Array.isArray({ x: 1, y: 2, z: 3 }));

// let test = {
//   x: 1,
//   y: 2,
//   z: 3,
// };

// console.log(Array.isArray({ test }));
// console.log(test);

// let test = [1, 2, 3];
// console.log(Array.isArray(test));

//
//
//
//
//
//
//
// 1

// alert("js".toUpperCase());

// alert("JS".toLowerCase());

// ......2
// let a = "я вчу JavaScript!";

// alert(" занимает " + a.length + " кодовых значений");

// .......3

// const str1 = "я вчу JavaScript!";

// alert(str1.substr(0, 1, 2, 3, 4, 5, 9));
// .......

// ..........
// 5
// alert("я вчу JavaScript!".indexOf("вчу"));
// // ...........
// let text = "після випуску курсів в дан it я буду гуру frontend";
// let newText = text.split(" ");
// console.log(newText.join("\n"));

// let countDawnDate = new Date("Jan 1, 23, 00:00:00").getTime();

// let x = setInterval(function () {
//   let now = new Date().getTime();
//   let distance = countDawnDate - now;
//   let days = math.floor(distance / (24 * 60 * 60 * 1000));
//   let hours = math.floor(distance / (24 * 60 * 1000));
//   let minutse = math.floor(distance / (60 * 1000));
//   let seconds = math.floor(distance / (60 * 1000));

// });

// class CountdownTimer {
//     constructor(deadline, cbChange, cbComplete) {
//       this._deadline = deadline;
//       this._cbChange = cbChange;
//       this._cbComplete = cbComplete;
//       this._timerId = null;
//       this._out = {
//         days: '', hours: '', minutes: '', seconds: '',
//         daysTitle: '', hoursTitle: '', minutesTitle: '', secondsTitle: ''
//       };
//       this._start();
//     }
//     static declensionNum(num, words) => {
//       return words[(num % 100 > 4 && num % 100 < 20) ? 2 : [2, 0, 1, 1, 1, 2][(num % 10 < 5) ? num % 10 : 5]];
//     }
//     _start() {
//       this._calc();
//       this._timerId = setInterval(this._calc.bind(this), 1000);
//     }
//     _calc() {
//       const diff = this._deadline - new Date();
//       const days = diff > 0 ? Math.floor(diff / 1000 / 60 / 60 / 24) : 0;
//       const hours = diff > 0 ? Math.floor(diff / 1000 / 60 / 60) % 24 : 0;
//       const minutes = diff > 0 ? Math.floor(diff / 1000 / 60) % 60 : 0;
//       const seconds = diff > 0 ? Math.floor(diff / 1000) % 60 : 0;
//       this._out.days = days < 10 ? '0' + days : days;
//       this._out.hours = hours < 10 ? '0' + hours : hours;
//       this._out.minutes = minutes < 10 ? '0' + minutes : minutes;
//       this._out.seconds = seconds < 10 ? '0' + seconds : seconds;
//       this._out.daysTitle = CountdownTimer.declensionNum(days, ['день', 'дня', 'дней']);
//       this._out.hoursTitle = CountdownTimer.declensionNum(hours, ['час', 'часа', 'часов']);
//       this._out.minutesTitle = CountdownTimer.declensionNum(minutes, ['минута', 'минуты', 'минут']);
//       this._out.secondsTitle = CountdownTimer.declensionNum(seconds, ['секунда', 'секунды', 'секунд']);
//       this._cbChange ? this._cbChange(this._out) : null;
//       if (diff <= 0) {
//         clearInterval(this._timerId);
//         this._cbComplete ? this._cbComplete() : null;
//       }
//     }
//   }

let timerInput = document.getElementById("time"); // Берём строку
let buttonRun = document.getElementById("button"); // Берём кнопку запуска
let timerShow = document.getElementById("timer"); // Берём блок для показа времени

buttonRun.addEventListener("click", function () {
  timeMinut = parseInt(timerInput.value) * 60;
});

timer = setInterval(function () {
  seconds = timeMinut % 60; // Получаем секунды
  minutes = (timeMinut / 60) % 60; // Получаем минуты
  hour = (timeMinut / 60 / 60) % 60; // Получаем часы
  // Условие если время закончилось то...
  if (timeMinut <= 0) {
    // Таймер удаляется
    clearInterval(timer);
    // Выводит сообщение что время закончилось
    alert("Время закончилось");
  } else {
    // Иначе
    // Создаём строку с выводом времени
    let strTimer = `${Math.trunc(hour)}:${Math.trunc(minuts)}:${seconds}`;
    // Выводим строку в блок для показа таймера
    timerShow.innerHTML = strTimer;
  }
  --timeMinut; // Уменьшаем таймер
}, 1000);
