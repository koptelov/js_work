// ARRAY МАССИВЫ

// let arr = ["a", "b", "c"];

// let arr1 = [1, 2, 3];

// console.log(arr);
// console.log(arr1);

// let arr3 = [1, 2, "a", "b", null, true, false];

// console.log(arr3);
// console.log(arr3.length);

// let arr4 = [12, 45, 1989, 2022, 24, 02];
// console.log(arr4);
// console.log(arr4.length);

// let arr5 = ["g", "e", "r", "m", "a", "n", "i", "a"];
// console.log(arr5);
// console.log(arr5.length);

// let arr = ["a", "b", "c", "d"];

// console.log(arr[0]);
// console.log(arr[1]);
// console.log(arr[2]);
// console.log(arr[3]);

// let arr1 = [1, 2, 3, 4];
// console.log(arr1[0]);
// console.log(arr1[1]);
// console.log(arr1[2]);
// console.log(arr1[3]);

// let arr = [1, 2, 3];

// alert(arr[0]);
// alert(arr[1]);
// alert(arr[2]);

// console.log(arr.indexOf(4));

// console.log(arr.lastIndexOf(3, 4));

// console.log(arr.includes(6));

// console.log(arr.concat([1, 2, 3, 4, 5, 6]));

// let arr = [1, 2, 3, 4, 5, 6];

// console.log(arr.slice(3));

// console.log(arr.splice(2, 1, 11, 12, 13));
// console.log(arr);

// const user1 = {
//   name: "serhii",
//   lastName: "Kopteilov",
//   age: 33,
//   country: "Germania",
//   city: "Bisschausen",
// };
// const user2 = {
//   name: "serhii",
//   lastName: "Kopteilov",
//   age: 33,
//   country: "Germania",
//   city: "Bisschausen",
// };
// const user3 = {
//   name: "serhii",
//   lastName: "Kopteilov",
//   age: 33,
//   country: "Germania",
//   city: "Bisschausen",
// };

// const arr = [user1, user2, user3];
// console.log(arr);

// const arr = ["Serhii", "Kolya", "Vitya", "Tanya", "Jora"];

// console.log(arr);

// let newArr = arr.slice(1, 4);

// let arr2 = ["a", "b", "c", "d"];
// console.log(arr2);

// console.log(arr2.join("+"));

// let arr = ["a", "b", "c"];
// console.log(arr.length);

// let arr = [1, 2, 3];

// console.log(arr[arr.length - 1]);

// let arr = [1, 2, 3, "string", true];
// console.log(arr.length);

// let arr = [1, 2, "String", true];

// console.log(arr[arr.length - 1]);

// let arr = ["a", "b", "c"];
// arr[0] = "string";
// console.log(arr);

// let arr = ["a", "b", "c"];
// arr[0] = 1;
// arr[1] = -2;
// arr[2] = -3;

// console.log(arr);

// let arr = ["a", "b", "c"];

// arr[0] = arr[0] + "!";
// arr[1] = arr[1] + "!";
// arr[2] = arr[2] + "!";

// console.log(arr);

// let arr = ["a", "b", "c", "d"];

// arr[0] += "!";
// arr[1] += "!";
// arr[2] += "!";
// arr[3] += "!";

// console.log(arr);

// let arr = [1, 2, 3, 4, 5];
// arr[0] += 3;
// arr[1] += 3;
// arr[2] += 3;
// arr[3] += 3;
// arr[4] += 3;
// console.log(arr);

// let arr = [1, 2, 3, 4];

// arr[0]++;
// ++arr[1];

// arr[2]--;
// --arr[3];

// console.log(arr);

// let arr = [1, 2, 3, 4];

// arr[0]++;
// arr[1]++;
// arr[2]++;
// arr[3]++;

// console.log(arr);

// let arr = [];

// arr[0] = "a";
// arr[1] = "b";
// arr[2] = "c";
// arr[3] = "d";
// arr[4] = "e";

// console.log(arr);

// let arr = ["a", "b", "c", "d"];

// arr[0] = "k";
// arr[4] = "y";

// console.log(arr);

// let arr = [];

// arr[0] = "a";
// arr[1] = "a";
// arr[2] = "a";
// arr[3] = "a";
// arr[4] = "a";

// console.log(arr);

// let arr = [1, 2, 3];

// arr[3] = 4;
// arr[4] = 5;

// console.log(arr);

// let arr = ["a", "b", "c"];

// arr[3] = "!";
// console.log(arr);

// let arr = [];

// arr[3] = "a";
// arr[8] = "a";
// console.log(arr.length);

// let arr = [];

// arr.push("a");
// arr.push("b");
// arr.push("c");

// console.log(arr);

// let arr = [];

// arr[0] = 1;
// arr[1] = 2;
// arr[2] = 3;
// arr[3] = 4;
// arr[4] = 5;

// arr[5] = 6;
// arr[6] = 7;
// console.log(arr);

// let arr = ["a", "b", "c"];

// console.log(arr[0]);
// console.log(arr[1]);

// let arr = ["serhii", "nadiia", "maria", "katarina"];

// const user1 = 0;

// console.log(arr[user1]);

// let arr = ["a", "b", "c"];

// let key = 2;

// console.log(arr[key]);

// let arr = [1, 2, 3, 4, 5];

// let key1 = 1;
// let key2 = 2;

// console.log(arr[1] + arr[2]);

// let arr = ["a", "b", "c", "d"];

// console.log(arr);

// delete arr[0];
// delete arr[2];
// console.log(arr);
// console.log(arr.length);

// let arr = [1, 2, 3, 4, 5];

// console.log(arr[0] + arr[1] + arr[2] + arr[3] + arr[4]);

// let arr = [1, 2, 3, 4, 5];
// console.log(arr.length);

// let arr = [1, 2, 3, 4, 5];
// console.log(arr.length);

// let obj = {
//   1: "a",
//   2: "b",
//   3: "c",
// };
// console.log(obj);

// let obj = {
//   1: "montag",
//   2: "dinstag",
//   3: "mitwoh",
//   4: "thensdey",
//   5: "fritag",
//   6: "sontag",
//   7: "sonnentag",
// };
// console.log(obj);

// let obj = {
//   1: "jenuary",
//   2: "february",
//   3: "marth",
//   4: "apriel",
//   5: "may",
//   6: "june",
//   7: "jule",
//   8: "august",
//   9: "semtember",
//   10: "oktober",
//   11: "november",
//   12: "desember",
// };

// console.log(obj);

// let obj = {
//   a: 1,
//   b: 2,
//   c: 3,
// };
// console.log(obj["a"]);

// let user = {
//   name: "Serhii",
//   lastName: "Koptielov",
//   country: "Germania",
// };
// console.log(user.name + " " + user.lastName + " " + user.country);

// let obj = {
//   a: 1,
//   b: 2,
//   c: 3,
// };
// console.log(obj.a + " " + obj.b + obj.c);

// let date = {
//   year: 2022,
//   month: "Desember",
//   day: 14,
// };
// console.log(date.year + "-" + date.month + "-" + date.day);

// let obj = {
//   "1key": "a",
//   "key-2": "b",
//   1: "c",
// };
// console.log(obj["1key"]);
// console.log(obj);

// let obj = {
//   "1a": 1,
//   b2: 2,
//   "с-с": 3,
//   "d 4": 4,
//   e5: 5,
// };
// console.log(obj);

// let obj = {
//   "1a": 1,
//   b2: 2,
//   "с-с": 3,
//   "d 4": 4,
//   e5: 5,
// };
// console.log(obj["1a"]);
// console.log(obj.b2);
// console.log(obj["с-с"]);
// console.log(obj["d 4"]);
// console.log(obj.e5);

// let obj = {
//   a: 1,
//   b: 2,
//   c: 3,
// };

// obj["a"] = "!";

// obj.b = "!";

// console.log(obj);

// let obj = {
//   x: 1,
//   y: 2,
//   z: 3,
// };
// console.log(obj);

// console.log(obj.x ** 2);
// console.log(obj.y ** 2);
// console.log(obj.z ** 2);

// let obj = {};

// obj["a"] = 1;
// obj["b"] = 2;
// obj["c"] = 3;

// console.log(obj);

// let obj = {};

// obj.a = 1;
// obj.b = 2;
// obj.c = 3;
// obj.d = 4;
// console.log(obj);

// let user = {};
// user.name = "serhii";
// user.lastName = "koptielov";
// user.country = "Germania";
// console.log(user);

// let user = {};

// user["name"] = "Serhii";
// user["lastName"] = "Serhii";
// user["country"] = "Serhii";
// console.log(user);

// let obj = {
//   3: "c",

//   2: "b",

//   1: "a",
// };
// console.log(obj[1]);
// console.log(obj[2]);
// console.log(obj[3]);

// let obj = {
//   a: 1,
//   b: 2,
//   c: 3,
// };
// let keys = Object.keys(obj);
// console.log(keys);

// console.log(keys[0]);
// console.log(keys[1]);
// console.log(keys[2]);

// delete keys[0];
// console.log(keys);

// let obj = {
//   x: 1,
//   y: 2,
//   z: 3,
// };

// let keys = Object.keys(obj);
// console.log(keys);

// let obj = {
//   a: 1,
//   b: 2,
//   c: 3,
//   d: 3,
//   k: 3,
//   n: 3,
// };
// console.log(Object.keys(obj).length);

// let obj = {
//   x: 1,
//   y: 2,
//   z: 3,
// };

// console.log(Object.keys(obj).length);

// let obj = {
//   a: 1,
//   b: 2,
//   c: 3,
// };

// let keys = "a";

// console.log(obj[keys]);

// let obj = {
//   x: 1,
//   y: 2,
//   z: 3,
// };

// let keys = "y";

// console.log(obj[keys]);

// let obj = { x: 1, y: 2, z: 3 };
// console.log(obj["x"]);

// let obj = { x: 1, y: 2, z: 3 };

// let key = "x";

// console.log(obj[key]);

// let obj = {
//   a: 1,
//   b: 2,
//   c: 3,
// };

// let key = "a";

// console.log(obj[key]);
// console.log(obj["a"]);

// let obj = { x: 1, y: 2, z: 3 };

// let prop = "x";
// console.log(obj[prop]);

// let obj = { x: 1, y: 2, z: 3 };

// let prop = "x";
// console.log(obj[prop]);

// let obj = {
//   a: 1,
//   b: 2,
//   c: 3,
// };

// let key = "a";

// let obj = {
//   [key]: 1,
//   b: 2,
//   c: 3,
// };
// console.log(obj);

// let key = "x";

// let obj = {
//   [key]: 1,
//   y: 2,
//   z: 3,
// };
// console.log(obj);

// let obj = {
//   x: 1,
//   y: 2,
//   z: 3,
// };

// let key1 = "x";
// let key2 = "y";
// let key3 = "z";

// console.log(obj[key1]);
// console.log(obj[key2]);
// console.log(obj[key3]);

// let obj = {
//   a: 1,
//   b: 2,
//   c: 3,
// };
// console.log("b" in obj);
// console.log("v" in obj);
// console.log("p" in obj);
// console.log("a" in obj);

// let obj = {
//   x: 1,
//   y: 2,
//   z: 3,
// };
// console.log("x" in obj);
// console.log("w" in obj);

// let obj = {
//   x: 1,
//   y: 2,
//   z: 3,
// };
// delete obj.x;
// console.log(obj);

// console.log("x" in obj);

// console.log(typeof { x: 1, y: 2, z: 3 });
// console.log(typeof {});

// let obj = { x: 1, y: 2, z: 3 };
// console.log(typeof obj);

// let obj = { x: 1, y: 2, z: 3 };
// console.log(typeof obj["x"]);

// console.log(typeof []);

// console.log(typeof [1, 2, 3]);

// console.log(typeof [1, 2, 3]);
// let arr = [1, 2, 3];

// console.log(typeof arr);
// let arr1 = [1, 2, 3];

// console.log(typeof arr1[0]);

// let arr = ["1", "2", "3"];
// console.log(typeof arr[0]);

// console.log(Array.isArray([]));

// let arr = [1, 2, 3];

// console.log(Array.isArray(arr));

// let test = { x: 1, y: 2, z: 3 };
// console.log(test);

// let test = { x: 1, y: 2, z: 3 };
// console.log(test.x);

// let test = [1, 2, 3];
// console.log(test[1]);

// let test1 = [1, 2, 3];
// let test2 = 1;

// console.log(test1[test2]);

// let obj1 = {
//   a: 1,
//   b: 2,
//   c: 3,
// };

// let obj2 = obj1;

// console.log(obj1);
// console.log(obj2);

// obj1["a"] = 2;

// console.log(obj1);
// console.log(obj2);

// obj2["b"] = 10;

// console.log(obj2);
// console.log(obj1);

// let arr1 = [1, 2, 3];
// let arr2 = arr1;

// arr1[0] = "a";
// console.log(arr2);

// let arr1 = [1, 2, 3];
// let arr2 = arr1;

// arr1[0] = "a";
// arr2[1] = "b";

// console.log(arr1);

// let arr1 = [1, 2, 3];
// let arr2 = arr1;

// arr1[0] = "a";
// arr2[0] = "b";

// console.log(arr2);

// const arr = ["a", "b", "c"];
// (arr[0] = 1), (arr[1] = 1), (arr[2] = 1);

// console.log(arr);

// let arr = [1, 2, 3, 4, 5];

// let res = arr[1] + arr[2];

// console.log(res);

// let obj = { x: 1, y: 2, z: 3 };
// console.log(obj.x);

// let obj = { x: 1, y: 2, z: 3 };
// let key = "x";

// console.log(obj[key]);

// let obj = { x: 1, y: 2, z: 3 };

// let sum = obj.x + obj.y + obj.z;

// console.log(obj);
// console.log(sum);

// let obj = { x: 1, y: 2, z: 3 };

// console.log(obj.length);

// console.log(Object.keys(obj));

// if (10 > 2) {
//   console.log("hellow");
// } else {
//   console.log("no hello");
// }

// let test = 6;

// if (test > 5) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let test = -1;

// if (test >= 0) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let age = +prompt("введите число");

// if (age >= 18) {
//   console.log("wellkome");
// } else {
//   console.log("go homo baby");
// }

// let ageIsus = +prompt("в каком возратсе убили Иссуса");

// if (ageIsus == 33) {
//   console.log("правильный ответ");
// } else {
//   console.log("не правильно");
// }

// let test = 1;

// if (test != 10) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let test1 = 10;
// let test2 = 2;

// if (test1 > test2) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let test1 = 2;
// let test2 = 4;

// if (test1 == test2) {
//   console.log("no");
// } else {
//   console.log("yes");
// }

// let test = "abc";

// if (test == "abc") {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let test1 = "123";
// let test2 = 123;

// if (test1 == test2) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// if ("3" === 3) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// if ("3" === "3") {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// if (3 != 3) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// if ("3" !== 3) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let test1 = "3";
// let test2 = "3";

// if (test1 != test2) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let test1 = "3";
// let test2 = "3";

// if (test1 !== test2) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let test1 = 3;
// let test2 = "3";

// if (test1 != test2) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let num = 8;

// if (num > 0 && num < 10) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let num1 = 2;
// let num2 = 3;

// if (num1 == 2 && num2 == 3) {
//   console.log("good");
// } else {
//   console.log("no");
// }

// let num = 5;

// if (num > 0 && num < 5) {
//   console.log("good");
// } else {
//   console.log("no");
// }

// let num = 221;

// if (num >= 10 && num <= 20) {
//   console.log("good");
// } else {
//   console.log("no");
// }

// let num1 = 1;
// let num2 = 3;
// let num3 = 7;

// if (num1 <= 1 && num2 >= 3 && num3 >= 7) {
//   console.log("good");
// } else {
//   console.log("no");
// }

// let num1 = 7;
// let num2 = 8;

// if (num1 >= 10 || num2 >= 2) {
//   console.log("good");
// } else {
//   console.log("no");
// }

// let num1 = -10;
// let num2 = -10;

// if (num1 >= 0 || num2 >= 0) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let num1 = 0;
// let num2 = 0;

// if (num1 >= 0 || num2 >= 0) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let num1 = 0;
// let num2 = 5;

// if (num1 >= 0 || num2 >= 0) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let num1 = -5;
// let num2 = 15;

// if (num1 >= 0 || num2 >= 0) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let num = 2;

// if (num == 0 || num == 1 || num == 2) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let num = 3;

// if ((num > 0 && num < 5) || (num > 10 && num < 20)) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let num = 3;

// if (num == 0 || (num > 1 && num < 5)) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let num = 8;

// if ((num > 0 && num < 5) || ((num) => 10 && num < 20)) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let num1 = 3;
// let num2 = 5;

// if (num1 > 0 && (num2 == 3 || num2 == 5)) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let num = 20;

// if ((num > 5 && num < 10) || num == 20) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let num = 3;

// if (num > 5 || (num > 0 && num < 3)) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let num = 29;

// if (num == 9 || (num > 10 && num < 20) || (num > 20 && num < 30)) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let num = 3;

// if ((num > 0 && num < 5) || (num <= 0 && num >= 5)) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let num = 3;
// if (num > 0 && num < 5) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let num1 = 3;
// let num2 = 3;

// if (!(num1 >= 0 || !num2 <= 10)) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let test = true;

// if (test == false) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let test = 1;
// if (test == true) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// console.log(Boolean(0)); // false
// console.log(Boolean(1)); // false
// console.log(Boolean(-0)); // false
// console.log(Boolean(+0)); // false
// console.log(Boolean(null)); // false
// console.log(Boolean(false)); // false
// console.log(Boolean(NaN)); // false
// console.log(Boolean(undefined)); // false
// console.log(Boolean("")); // false

// console.log(NaN == false);

// let test = Boolean(-1);
// console.log(test);

// let test = 1;

// if (test != true) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let test = "";

// if (test == false) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let test;

// if (test == true) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let test = ;

// if (test == true) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// console.log(Boolean(3 * "a"));

// let test = true;

// if (test == true) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let test1 = true;

// if (test) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let test = true;

// if (test) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let test = true;

// if (!test) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let test = true;

// if (!test) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let test = true;

// if (test) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let test = 3;

// if (test) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let test1 = 3;

// if (Boolean(test1) === true) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let test = "abc";

// if (test) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let test = "";

// if (test) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let test = 3 * "abc";

// if (test) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let test = null;

// if (test) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let test = false;

// if (test) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let test;

// if (test) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let test = "0";

// if (test) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let test = -1;

// if (test) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let test1 = false;
// let test2 = false;

// if (!test1 && !test2) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let test1 = true;
// let test2 = true;

// if (!test1 || !test2) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let test1 = true;
// let test2 = true;

// if (test1 && test2) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let test1 = true;
// let test2 = true;

// if (test1 && !test2) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let test1 = true;
// let test2 = true;

// if (!test1 && !test2) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let test1 = true;
// let test2 = true;

// if (test1 && test2) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let test1 = true;
// let test2 = true;
// let test3 = true;

// if (test1 && test2 && test3) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let test1 = true;
// let test2 = true;
// let test3 = true;

// if (test1 || (test2 && test3)) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let test1 = true;
// let test2 = true;
// let test3 = true;

// if (test1 || (!test2 && !test3)) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let test = 1;

// if (test !== 1) {
//   console.log("+++");
// }

// let test = 10;

// if (test == 10) {
//   console.log("yes");
// }

// let test = 10;

// if (test === 0) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// if (test === 0) console.log("+++");
// else console.log("---");

// if (test === 0) {
//   console.log("+++");
// } else "---";

// let test = 0;

// if (test > 0) console.log("+++");
// else console.log("---");

// let test = 10;

// if (test > 0) console.log("+++");

// let num = 5;

// if (num == 1) {
//   console.log("VALUE1");
// } else if (num == 2) {
//   console.log("value2");
// } else if (num == 3) {
//   console.log("value3");
// } else {
//   console.log("no");
// }

// let day = 10;

// if (day < 0) {
//   console.log("do 10");
// } else if (day >= 10) {
//   console.log("do 20");
// } else (day >= 20) {
//     console.log("do 31");
// }

// let num = 1;

// if (num > 0) {
//   console.log("value1");
// } else if (num > 10) {
//   console.log("value2");
// } else num > 20;
// {
//   console.log("value3");
// }

// let day = 32;

// if (day > 0 && day < 10) {
//   console.log("do 10");
// } else if (day > 10 && day <= 20) {
//   console.log("do 20");
// } else if (day > 20 && day <= 31) {
//   console.log("do 31");
// } else {
//   console.log("no number");
// }

// let num = 6;

// if (num >= 0) {
//   if (num <= 5) {
//     console.log("small 5 || ==");
//   } else {
//     console.log("big 5");
//   }
// } else {
//   console.log("small 0");
// }

// let num = 6;

// if ((num) => 0) {
//   if (num == 0) {
//     console.log("0");
//   } else if (num == 1) {
//     console.log("1");
//   } else if (num == 2) {
//     console.log("2");
//   } else if (num == 3) {
//     console.log("3");
//   } else if (num == 4) {
//     console.log("4");
//   } else if (num == 5) {
//     console.log("5");
//   } else {
//     console.log("small 0 un big 6");
//   }
// }

// let num = 10;

// if (num > 10 && num >= 99) {
//   console.log("small 10");
// } else {
//   let num2 = num % 10;
//   let num1 = (num - num2) / 10;
//   let sum = num1 + num2;
//   console.log(sum);
// }
// if (sum <= 9) {
//   console.log("odnoznachno");
// } else {
//   console.log("suma dvhznfchna");
// }

// let num = 0;
// switch (num) {
//   case 1:
//     console.log("value1");
//     break;
//   case 2:
//     console.log("value2");
//     break;
//   case 3:
//     console.log("value3");
//     break;
//   default:
//     console.log("incorrect value");
//     break;
// }

// let lang = "en";

// switch (lang) {
//   case "ru":
//     console.log("rus");
//     break;

//   case "en":
//     console.log("england");
//     break;

//   case "de":
//     console.log("germania");
//     break;
//   default:
//     console.log("icorekt value");
// }

// let num = 5;

// switch (num) {
//   case 1:
//     console.log("1");
//     break;
//   case 2:
//     console.log("2");
//     break;
//   case 3:
//     console.log("3");
//     break;
//   case 4:
//     console.log("4");
//     break;
//   case 5:
//     console.log("5");
//     break;
// }

// let num = 9;
// let res;

// switch (num) {
//   case 1:
//   case 2:
//     res = "a";
//     break;
//   case 3:
//     res = "b";
//     break;
//   default:
//     console.log("no");
// }
// console.log(res);

// let num = 3;
// num === 3 ? console.log("good") : console.log("no");

// let age = 17;
// let adult = age >= 18 ? true : false;
// console.log(adult);

// let num = -1;
// let res = num >= 0 ? "1" : "2";

// console.log(res);

// let num = 10;
// let res;

// if (num >= 0) {
//   res = "1";
// } else {
//   res = "2";
// }

// console.log(res);

// console.log("sd");

// console.log(1 == 1);
// console.log(2 == 2);

// let a = 1;
// let b = 2;
// let c = a == b;

// console.log(c);

// let a = 2 * (3 - 1);
// let b = 6 - 2;

// let c = b == a;
// console.log(c);

// let a = 5 * (7 - 4);
// let b = 1 + 2 + 7;

// console.log(a > b);

// let a = 2 ** 4;
// let b = 4 ** 2;

// console.log(a != b);

// let ok = confirm("текст вопроса");

// if (ok == true) {
//   console.log("true");
// } else {
//   console.log("false");
// }

// let ok = confirm("текст");

// if (ok) {
//   console.log("tak");
// } else {
//   console.log("no");
// }

// let userAge = confirm("how old you 18?");

// if (userAge) {
//   alert("wellkome");
// } else {
//   alert("no servise, good bay");
// }

// let res = "!";
// if (true) {
//   console.log(res);
// }

// let res;

// if (true) {
//   res = "!";
// }
// console.log(res);

// let num = 0;
// let res;

// if (num == 0) {
//   let res = 1;
// } else {
//   let res = 2;
// }
// console.log(res);

// let age = 17;
// let adult;

// if (age >= 18) {
//   adult = "good";
// } else {
//   adult = "no";
// }

// console.log(adult);

// let test = true;
// let res; // объявим переменную снаружи условия

// if (test) {
//   res = 1;
// } else {
//   res = 2;
// }

// console.log(res); // выведет 1

// let res = 1;

// if (true) {
//   res = 2;
// }
// console.log(res);

// let age = 21;
// let adult;

// if (age >= 18) {
//   adult = true;
// } else {
//   adult = false;
// }

// console.log(adult);

// let age = 21;
// let adult;

// if (age >= 18) {
//   adult = true;
// } else {
//   adult = false;
// }

// console.log(adult);

// let age = 17;
// let res;

// if (age >= 18) {
//   if (age <= 23) {
//     res = "от 18 до 23";
//   } else {
//     res = "больше 23";
//   }
// } else {
//   res = "меньше 18";
// }

// console.log(res);

// let age = 12;
// let res;

// if (age >= 18) {
//   if (age <= 23) {
//     res = "от 18 до 23";
//   } else {
//     res = "больше 23";
//   }
// } else {
//   res = "меньше 18";
// }

// console.log(res);

// let min = 29;

// if (min >= 0 && min <= 19) {
//   console.log("1 четверть");
// }
// if (min >= 20 && min <= 39) {
//   console.log("2 четверть");
// }
// if (min >= 40 && min <= 59) {
//   console.log("3 четверть");
// }

// let str = "12345";
// console.log(str.length);

// let str = "12345";

// if (str.length >= 6) {
//   console.log("yes");
// } else {
//   console.log("no");
// }

// let arr = [1, 2, 3, 4, 5];

// if (arr.length == 5) {
//   console.log(arr.length);
// }

// let str = "12345";

// if (str[0] == 1) {
//   console.log("yes");
// }

// let str = "12345";

// let last = str[str.length - 1];

// if (last == 5) {
//   console.log("yes");
// }

// let str = "hello";

// if (str[0] == "h") {
//   console.log("yes");
// } else {
//   console.log("no");
// }

// let str = "world";

// console.log(str.length);

// if (str[4] == "x") {
//   console.log("yes");
// } else {
//   console.log("no");
// }

// let str = "world";

// str[4] === "x" ? console.log("yes") : console.log("no");

// let str = "berhii";

// if (str[0] === "a" || str[0] === "b") {
//   console.log("yes");
// }

// let num = 12345;
// let str = String(num);

// if (str[0] == 1) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let num = 12345;

// if (String(num)[0] == 1 || String(num)[0] == 2) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let num = 12345;
// let str = String(num)[0];

// if (str[0] == 1 || str[0] == 2) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let num = 12345;

// let str = String(num)[4];

// if (str == 5) {
//   console.log("+++");
// } else {
//   console.log("---");
// }
// console.log(str.length);

// let num = 4000;

// if (num % 2 == 0) {
//   console.log("yes");
// } else {
//   console.log("no");
// }

// let a = 10;
// let b = 3;

// a % b == 0 ? console.log("yes") : console.log("no");

// let a = 10;
// let b = 3;

// if (a % b === 0) {
//   console.log("delitsa celno");
// } else {
//   console.log("s octatkom" + " " + (a % b));
// }

// let a = 10;
// let b = 3;
// let c = a % b;

// if (c === 0) {
//   console.log("delitsa celno");
// } else {
//   console.log("delitsa s ostatkom" + " " + c);
// }

// let num = 2022;

// if (!!num % 2 == 0) {
//   console.log(" chotnoe");
// } else {
//   console.log(" ne chotnoe");
// }
// console.log(!!num % 2 === 0);

// let num = 21;

// if (num % 3 == 0) {
//   console.log("good");
// } else {
//   console.log("no");
// }

// let num1 = 1;
// let num2 = 2;

// if (num1 + num2 === 3) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let num1 = 1;
// let num2 = 2;

// if (num1 + num2 === 3) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let num1 = +"1";
// let num2 = +"2";

// if (num1 + num2 == 3) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// console.log(typeof num1);
// console.log(typeof num2);

// let num = 123;

// if (String(num)[0] == 1) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let num = 123;

// if (String(num)[0] == 1) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let num = 123;

// if (String(num)[0] == 1) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let num = 123;
// let first = String(num)[0];

// if (first == 1) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let num = 12;

// if (String(num).length === 2) {
//   console.log("+++");
// } else {
//   console.log("---");
// }
// console.log(String(num).length);

// let num = 99;

// if (String(num).length === 2) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let num = 12;

// if (String(num).length === 2) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let num = 12;

// if (String(num).length === 2) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let num = 12;

// if (String(num).length === 2) {
//   console.log("+++");
// } else {
//   console.log("---");
// }

// let num = "345346";

// let sum1 = num[0] + num[1] + num[2];
// let sum2 = num[3] + num[4] + num[5];

// if (sum1 == sum2) {
//   console.log("суммы равны");
// } else {
//   console.log("суммы не равны");
// }

// let min = 1;

// if (min >= 0 && min <= 19) {
//   console.log("1 четверть");
// }
// if (min >= 20 && min <= 39) {
//   console.log("2 четверть");
// }
// if (min >= 40 && min <= 59) {
//   console.log("3 четверть");
// }

// let month = 5;

// if (month >= 1 && month <= 3);
// {
//   console.log("winter");
// }

// if (month >= 4 && month <= 6) {
//   console.log("vesna");
// }

// if (month >= 7 && month <= 9) {
//   console.log("leto");
// }

// if (month >= 10 && month <= 12) {
//   console.log("osen");
// }

// let str = "abcdw";

// if (str[0] == "a") {
//   console.log("yes");
// } else {
//   console.log("no");
// }

// let num = 12345;

// let str = String(num);

// if (str[0] == 1 || str[0] == 2 || str[0] == 3) {
//   console.log("yes");
// } else {
//   console.log("no");
// }

// let num = String(123);

// console.log(Number(num[0]) + Number(num[1]) + Number(num[2]));

// let num = String(456456);

// let num1 = Number(num[0]) + Number(num[1]) + Number(num[2]);
// let num2 = Number(num[3]) + Number(num[4]) + Number(num[5]);

// if (num1 == num2) {
//   console.log("yes");
// } else {
//   console.log("no");
// }

// for (let i = 1; i <= 9; i++) {
//   console.log(i);
// }

// for (let i = 1; i <= 9; i += 2) {
//   console.log(i);
// }

// for (let i = 10; i >= 0; i--) {
//   console.log(i);
// }

// for (let i = 1; i <= 100; i++) {
//   console.log(i);
// }

// for (let i = 11; i <= 33; i++) {
//   console.log(i);
// }

// for (let i = 0; i <= 100; i += 2) {
//   console.log(i);
// }

// for (let i = 1; i <= 99; i += 2) {
//   console.log(i);
// }

// for (let i = 100; i >= 0; i--) {
//   console.log(i);
// }

// let arr1 = [1, 2, 3, 4, 5];

// for (let elem1 of arr1) {
//   console.log(elem1);
// }

// let arr = ["a", "b", "c", "d", "e"];

// for (let elem of arr) {
//   console.log(elem);
// }

// let obj = {
//   userName: "Serhii",
//   userLastName: "Koptelov",
//   userAge: 33,
// };

// for (let key in obj) {
//   console.log(key);
// }

// for (let key in obj) {
//   console.log(obj[key]);
// }

// let obj = {
//   x: 1,
//   y: 2,
//   z: 3,
// };

// for (let key in obj) {
//   console.log(key);
// }

// for (let key in obj) {
//   console.log(obj[key]);
// }

// let i = 1;

// while (i <= 100) {
//   console.log(i);
//   i++;
// }

// let num = 500;

// while (num > 10) {
//   num = num / 2;
// }

// let i = 1;

// while (i <= 1000) {
//   console.log(i);
//   i++;
// }

// let num = 500;

// while (num > 10) {
//   num = num / 2;
// }

// console.log(num);

// let num1 = 7.8125;

// while (num1 < 500) {
//   num1 = num1 * 2;
//   console.log(num1);
// }

// let num = 1;

// while (num < 100) {
//   num++;
//   console.log(num);
// }

// let num = 1;

// while (num <= 100) {
//   console.log(num);
//   num++;
// }

// let num = 11;

// while (num <= 33) {
//   console.log(num);
//   num++;
// }

// let num = 3;

// while (num < 1000) {
//   num = num * 3;
//   console.log(num);
// }

// let arr = [1, 2, 3, 4, 5, 6, 7, 8];

// for (let i = 0; i < arr.length; i++) {
//   console.log(arr[i]);
// }

// let arr = [1, 2, 3, 4, 5, 6, 7, 8];

// for (let i = 0; i < arr.length; i++) {
//   console.log(arr[i]);
// }

// let arr = [1, 2, 3, 4, 5, 6, 7, 8];

// for (let i = arr.length - 1; i >= 0; i--) {
//   console.log(arr[i]);
// }

// let arr = ["a", "b", "c", "d", "e", "f"];

// for (let i = 0; i < arr.length; i++) {
//   console.log(arr[i]);
// }

// let arr = ["a", "b", "c", "d", "e"];

// for (let i = arr.length - 1; i >= 0; i--) {
//   console.log(arr[i]);
// }

// let arr = ["a", "b", "c", "d", "e"];

// for (let i = 0; i < arr.length; i++) {
//   console.log(arr[i]);
// }

// let arr = [1, 2, 3, 4, 5];

// for (let elem of arr) {
//   if (elem % 2 === 0) {
//     console.log(elem);
//   }
// }

// let arr = [2, 5, 9, 15, 1, 4];

// for (let elem of arr) {
//   if (elem >= 3 && elem < 10) {
//     console.log(elem);
//   }
// }

// let obj = {
//   a: 1,
//   b: 2,
//   c: 3,
//   d: 4,
//   e: 5,
// };

// for (let key in obj) {
//   console.log(obj[key]);
// }

// for (let key in obj) {
//   console.log(obj[key]);
// }

// let arr = [1, 2, 3, 4, 5];

// console.log(arr);

// let arr2 = ["a", "g", "c"];
// console.log(arr2);

// let arr = [1, 2, 3];
// console.log(arr[0]);
// console.log(arr[1]);
// console.log(arr[2]);

// let arr = [1, 2, 3];

// console.log(arr[0]);
// console.log(arr[1]);
// console.log(arr[2]);

// let arr = [1, 2, 3];

// console.log(arr[0] + arr[1] + arr[2]);

// let arr = ["a", "b", "c", "d"];

// console.log(arr[0] + arr[1] + arr[2] + arr[3]);

// _______________________
// _______________________
// _______________________
// _______________________

// 1111111111
// var A1 = []; // исходный массив
// var A2 = []; // результирующий массив

// // 2. Задать количество элементов в массиве A1
// A1.length = prompt("n = ");

// // 3. Ввести элементы массива A1 в цикле
// for (var i = 0; i < A1.length; i++) A1[i] = prompt("A1[" + i + "] = ");

// // 4. Вывести n и исходный массив A1
// document.write("n = " + A1.length + "<br>");

// document.write("A1:<br>");
// for (var i = 0; i < A1.length; i++) document.write(A1[i] + " ");
// document.write("<br>");

// // 4. Сформировать сжатый массив A2

// var j = 0; // текущее количество элементов в массиве A2

// for (var i = 0; i < A1.length; i++) if (A1[i] < 0) A2[j++] = A1[i];

// // 5. Вывести массив A2
// document.write("A2:<br>");
// for (var i = 0; i < A2.length; i++) document.write(A2[i] + " ");
// document.write("<br>");

// Одномерные массивы. Решение задач.

// Разделение массива на части

// 222222222222222222222222
// ???????????????????????????????
// ???????????????????????????????
// ???????????????????????????????
// ???????????????????????????????
// 22222222222222222222222222222
// 1. Объявление массивов
// let A = []; // исходный массив
// let B = [],
//   C = new Array(); // результирующие массивы

// // 2. Задать количество элементов в массиве A
// A.length = prompt("n = ");

// // 3. Ввести элементы массива A в цикле
// for (var i = 0; i < A.length; i++) A[i] = prompt("A[" + i + "] = ");

// // 4. Вывести n и исходный массив A1
// document.write("n = " + A.length + "<br>");

// document.write("A:<br>");
// for (var i = 0; i < A.length; i++) document.write(A[i] + " ");
// document.write("<br>");

// 4. Сформировать массивы B, C,
// массив B - положительные элементы, массив C - отрицательные элементы.
// var nb = 0; // текущее количество элементов в массиве B
// var nc = 0; // текущее количество элементов в массиве C

// for (var i = 0; i < A.length; i++)
//   if (A[i] >= 0) B[nb++] = A[i];
//   else C[nc++] = A[i];

// // 5. Вывести массив B
// document.write("B:<br>");
// for (var i = 0; i < B.length; i++) document.write(B[i] + " ");
// document.write("<br>");

// // 6. Вывести массив C
// document.write("C:<br>");
// for (var i = 0; i < C.length; i++) document.write(C[i] + " ");
// document.write("<br>");

/**
 *
 *
 *
 *
 *
 */

// 3333333
// Одномерные массивы. Решение задач.

// Объединение массивов B = A1 + A2

// // 1. Объявление массивов и их инициализация случайными значениями
// var A1 = new Array(),
//   A2 = new Array(); // исходные массивы
// var B = []; // результирующий массив

// // 2. Задать количество элементов в массиве A1
// A1.length = prompt("A1.length = ");

// // 3. Задать количество элементов в массиве A2
// A2.length = prompt("A2.length = ");

// // 4. Заполнить массивы A1, A2 случайными числами в интервале [-10; 10]
// for (var i = 0; i < A1.length; i++)
//   A1[i] = parseInt(-10 + Math.random() * (10 - -10 + 1));

// for (var i = 0; i < A2.length; i++)
//   A2[i] = parseInt(-10 + Math.random() * (10 - -10 + 1));

// // 5. Вывести массив A1 для контроля
// document.write("A1:<br>");
// for (var i = 0; i < A1.length; i++) document.write(A1[i] + "\t");
// document.write("<br><br>");

// // 6. Вывести массив A2 для контроля
// document.write("A2:<br>");
// for (var i = 0; i < A2.length; i++) document.write(A2[i] + "\t");
// document.write("<br><br>");

// // 7. Сформировать результирующий массив B

// // Сформировать длину массива B
// B.length = A1.length + A2.length;

// // Записать элементы массива A1
// for (var i = 0; i < A1.length; i++) B[i] = A1[i];

// // Записать элементы массива A2
// for (var i = 0; i < A2.length; i++) B[A1.length + i] = parseInt(A2[i]);

// // 8. Вывести массив B
// document.write("B:<br>");
// for (var i = 0; i < B.length; i++) document.write(B[i] + " ");
// document.write("<br>");

// let arr = ["a", "b", "c", "d"];

// console.log(arr.join("+"));

// let arr = ["a", "b", "c"];
// console.log(arr.length);

// // минус это вывод с конца
// console.log(arr[arr.length - 3]);

// let arr = [1, 2, 3, 4, 5];

// console.log(arr.length);
// console.log(arr.length - 0);

// let arr = [1, 2, 3, 4, 5];

// console.log(arr.length - 0);

// let arr = ["a", "b", "c"];

// arr[0] = 1;
// console.log(arr);

// let arr = ["a", "b", "c"];

// arr[0] = 1;
// arr[1] = -2;
// arr[2] = -3;

// console.log(arr);

// arr[0] = arr[0] + "!";
// arr[1] = arr[1] + "!";
// arr[2] = arr[2] + "!";

// console.log(arr);

// let arr = [1, 2, 3];

// arr[0] = arr[0] + "3";
// arr[1] = arr[1] + "3";
// arr[2] = arr[2] + "3";

// console.log(arr);

// let arr = [1, 2, 3, 4];

// arr[0]++;
// ++arr[1];

// arr[2]--;
// --arr[2];

// console.log(arr);

// let arr1 = [1, 2, 3, 4];

// arr1[0]++;
// arr1[1]++;
// arr1[2]++;
// arr1[3]++;

// console.log(arr1);

// --arr1[0];
// --arr1[1];
// --arr1[2];
// --arr1[3];

// console.log(arr1);

// let arr = [1, 2, 3];
// console.log(arr);
// arr[0]++;
// arr[1]++;
// arr[2]++;
// console.log(arr);

// let arr1 = [];

// arr1[0] = "a";
// arr1[1] = "b";
// arr1[2] = "c";

// let arr = [];

// arr[0] = "a";
// arr[1] = "b";
// arr[2] = "c";

// console.log(arr);
// console.log(arr1);

// let arr = ["a", "b", "c"];

// arr[3] = "d";
// console.log(arr);

// let arr = [];

// arr[0] = 1;
// arr[1] = 2;
// arr[2] = 3;

// console.log(arr);

// let arr = [1, 2, 3];

// arr.push(4, 5);

// console.log(arr);

// let arr = [1, 2, 3, 4];

// arr[8] = 9;
// console.log(arr);

// console.log(arr.length);

// let arr = [];

// arr[3] = "a";
// arr[8] = "b";

// console.log(arr);
// console.log(arr.length);

// let arr = [];

// arr.push("a");
// arr.push("b");
// arr.push("c");

// console.log(arr);

// let arr = [];

// arr.push("a");
// arr.push("b");
// arr.push("c");

// console.log(arr);

// arr.push(4, 5);
// console.log(arr);

// let arr = ["a", "b", "c"];

// console.log(arr[0]);

// let key = 0;

// console.log(arr[key]);

// let arr = ["a", "b", "c"];

// let key = 2;

// console.log(arr[key]);

// let arr = [1, 2, 3, 4, 5];

// let key1 = 1;
// let key2 = 2;

// let result = arr[key1] + arr[key2];

// // console.log(arr[key1] + arr[key2]);
// console.log(result);

// let arr = ["a", "b", "c"];

// console.log(arr);

// delete arr[2];

// console.log(arr);

// let arr = [];

// arr[0] = "a";
// arr[1] = "b";
// arr[2] = "c";

// console.log(arr);

// delete arr[0];

// console.log(arr);

// let arr = [1, 2, 3, 4, 5];

// console.log(arr.length);

// delete arr[0];
// delete arr[1];

// console.log(arr);

// let arr = [1, 2, 3, 4, 5];

// console.log(arr.length);

// let arr = [1, 2, 3, 4, 5];
// console.log(arr[0] + arr[1] + arr[2] + arr[3] + arr[4]);

// console.log(arr);

// let arr = [1, 2, 3, 4, 5];

// console.log(arr.length);

// let arr = [1, 2, 3, 4, 5];

// console.log(arr.length);

// for (let i = 1; i <= 9; i += 2) {
//   console.log(i);
// }

// for (let i = 10; i > 0; i--) {
//   console.log(i);
// }

// for (let i = 1; i < 100; i++) {
//   console.log(i);
// }

// for (let i = 11; i <= 33; i++) {
//   console.log(i);
// }

// for (let i = 0; i <= 100; i += 2) {
//   console.log(i);
// }

// for (let i = 1; i <= 99; i += 2) {
//   console.log(i);
// }

// for (let i = 100; i > 0; i--) {
//   console.log(i);
// }

// let arr = [1, 2, 3, 4, 5, 6, 7, 8, 9];
// console.log(arr.length);

// for (let elem of arr) {
//   console.log(elem);
// }

let arr = ["a", "b", "c", "d"];

for (let arrElement of arr) {
  console.log(arrElement);
}
